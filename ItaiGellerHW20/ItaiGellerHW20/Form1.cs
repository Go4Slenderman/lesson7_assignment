﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using ItaiGellerHW20.Properties;

namespace ItaiGellerHW20
{
    public partial class Form1 : Form
    {
        private const int cardWidth = 80;
        private const int cardHeight = 96;
        private NetworkStream clientStream; //Used to send messages through other functions
        private bool flipped = true;
        private int val = 0;
        private int thisScore = 0;
        private int opponentScore = 0;
        private PictureBox thisCard;
        private bool exitFlag = false;

        public Form1()
        {
            InitializeComponent();
            MessageBox.Show("NOTE: If you do not understand who won the round, please watch the scores");
            Thread connection = new Thread(HandleConnection); //Connecting to the server
            connection.Start();
        }

        private void EnableButtons(bool flag)
        {
            btnForfeit.Enabled = flag; //Enabling/Disabling the forfeit button
            foreach(Object picture in Controls) //Enabling/Disabling each card
            {
                if (picture is PictureBox)
                    ((PictureBox)picture).Enabled = flag;
            }
        }

        private void GenerateCards()
        {
            Point nextLocation = new Point(10, 210); //First card's initial location
            for (int counter = 0; counter < 10; counter++) //Generate 10 cards
            {
                System.Windows.Forms.PictureBox currPic = new PictureBox();
                currPic.Name = "picDynamic" + counter;
                currPic.Image = global::ItaiGellerHW20.Properties.Resources.card_back_red; //Setting the card as a turned red card
                currPic.Location = nextLocation; //Setting the location of the card
                currPic.Size = new System.Drawing.Size(cardWidth, cardHeight);
                currPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage; //Setting the card to being stretched
                currPic.Click += delegate(object sender1, EventArgs e1)
                {
                    /*string currIndex = currPic.Name.Substring(currPic.Name.Length - 1);
                    MessageBox.Show("You've clicked card #" + currIndex);*/
                    Random r = new Random(); //Picking the value and type of the card
                    int randCard = r.Next(0, 13) + 1;
                    string cardStr = "1" + ((randCard < 10) ? "0" + randCard.ToString() : randCard.ToString()); //Preparing the string to send
                    switch(r.Next(0, 4) + 1)
                    {
                        case 1: //Heart
                            cardStr += "H";
                            break;
                        case 2: //Club
                            cardStr += "C";
                            break;
                        case 3: //Spade
                            cardStr += "S";
                            break;
                        case 4: //Diamond
                            cardStr += "D";
                            break;
                    }
                    byte[] buffer = new byte[4];
                    buffer = new ASCIIEncoding().GetBytes(cardStr);
                    clientStream.Write(buffer, 0, 4); //Sending the message
                    clientStream.Flush();
                    System.Resources.ResourceManager RM = new System.Resources.ResourceManager("ItaiGellerHW20.Properties.Resources", typeof(Resources).Assembly);
                    ((PictureBox)sender1).Image = (Image)RM.GetObject(ParseCard(cardStr));
                    flipped = false;
                    val = randCard;
                    thisCard = ((PictureBox)sender1);
                    /*((PictureBox)sender1).Hide();
                    ((PictureBox)sender1).Dispose();*/
                };
                Controls.Add(currPic); //Displaying the new card
                nextLocation.X += cardWidth + 10; //Updating the location of the next card
            }
        }

        private string ParseCard(string code)
        {
            string card = "";
            switch(code.Substring(1, 2)) //Getting the card value
            {
                case "01": //Ace
                    card += "ace";
                    break;
                case "11": //Jack
                    card += "jack";
                    break;
                case "12": //Queen
                    card += "queen";
                    break;
                case "13": //King
                    card += "king";
                    break;
                default: //Any numbered card
                    card += "_" + (int.Parse(code.Substring(1, 2)));
                    break;
            }
            card += "_of_";
            switch(code.Last()) //Getting the card type
            {
                case 'H': //Heart
                    card += "hearts";
                    break;
                case 'C': //Club
                    card += "clubs";
                    break;
                case 'S': //Spade
                    card += "spades";
                    break;
                case 'D': //Diamond
                    card += "diamonds";
                    break; 
            }
            return card;
        }

        private void HandleConnection()
        {
            string input = "";
            try
            {
                TcpClient client = new TcpClient(); //Connecting to the server
                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
                client.Connect(serverEndPoint);
                clientStream = client.GetStream();
                int bytesRead = 0;

                while (input != "2000")
                {
                    byte[] bufferIn = new byte[4]; //Recieving a message
                    bytesRead = clientStream.Read(bufferIn, 0, 4);
                    input = new ASCIIEncoding().GetString(bufferIn);
                    if (input == "0000") //If the game has started generate each players' cards
                    {
                        EnableButtons(true);
                        Invoke((MethodInvoker)delegate { GenerateCards(); });
                    }
                    else
                    {
                        System.Resources.ResourceManager RM = new System.Resources.ResourceManager("ItaiGellerHW20.Properties.Resources", typeof(Resources).Assembly);
                        pbOpponentCard.Image = (Image)RM.GetObject(ParseCard(input));
                        int opponentVal = int.Parse(input.Substring(1, 2));
                        while (flipped) {}
                        if (val > opponentVal)
                        {
                            thisScore++;
                            MessageBox.Show("You won the round");
                            lblMyScore.Text = "Your Score: " + thisScore.ToString();
                        }
                        else if (val < opponentVal)
                        {
                            opponentScore++;
                            MessageBox.Show("The opponent won the round");
                            lblOpponentScore.Text = "Opponent's Score: " + opponentScore.ToString();
                        }
                        else
                            MessageBox.Show("Nobody won the round");

                        thisCard.Image = (Image)RM.GetObject("card_back_red");
                        pbOpponentCard.Image = (Image)RM.GetObject("card_back_blue");
                        flipped = true;
                    }
                }
                EndGame();
            }
            catch(SocketException e) //If the connection with the server could not be established
            {
                MessageBox.Show("Please run the server before running the clients.");
                exitFlag = true;
                Close();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exitFlag)
                EndGame();
        }

        private void EndGame()
        {
            MessageBox.Show("Your Score: " + thisScore + "\nOpponent's Score: " + opponentScore);
            if (thisScore > opponentScore) //If the player won
                MessageBox.Show("You won the game!");
            else if (thisScore < opponentScore) //If the opponent won
                MessageBox.Show("The opponent won the game!");
            else
                MessageBox.Show("It's a tie!");
        }

        private void btnForfeit_Click(object sender, EventArgs e)
        {
            EndGame();
            exitFlag = true;
            Application.Exit();
        }
    }
}
