﻿namespace ItaiGellerHW20
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMyScore = new System.Windows.Forms.Label();
            this.lblOpponentScore = new System.Windows.Forms.Label();
            this.btnForfeit = new System.Windows.Forms.Button();
            this.pbOpponentCard = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbOpponentCard)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMyScore
            // 
            this.lblMyScore.AutoSize = true;
            this.lblMyScore.Location = new System.Drawing.Point(12, 9);
            this.lblMyScore.Name = "lblMyScore";
            this.lblMyScore.Size = new System.Drawing.Size(72, 13);
            this.lblMyScore.TabIndex = 0;
            this.lblMyScore.Text = "Your Score: 0";
            // 
            // lblOpponentScore
            // 
            this.lblOpponentScore.AutoSize = true;
            this.lblOpponentScore.Location = new System.Drawing.Point(794, 9);
            this.lblOpponentScore.Name = "lblOpponentScore";
            this.lblOpponentScore.Size = new System.Drawing.Size(104, 13);
            this.lblOpponentScore.TabIndex = 1;
            this.lblOpponentScore.Text = "Opponent\'s Score: 0";
            // 
            // btnForfeit
            // 
            this.btnForfeit.Enabled = false;
            this.btnForfeit.Location = new System.Drawing.Point(429, 4);
            this.btnForfeit.Name = "btnForfeit";
            this.btnForfeit.Size = new System.Drawing.Size(75, 23);
            this.btnForfeit.TabIndex = 2;
            this.btnForfeit.Text = "Forfeit";
            this.btnForfeit.UseVisualStyleBackColor = true;
            this.btnForfeit.Click += new System.EventHandler(this.btnForfeit_Click);
            // 
            // pbOpponentCard
            // 
            this.pbOpponentCard.Image = global::ItaiGellerHW20.Properties.Resources.card_back_blue;
            this.pbOpponentCard.Location = new System.Drawing.Point(426, 45);
            this.pbOpponentCard.Name = "pbOpponentCard";
            this.pbOpponentCard.Size = new System.Drawing.Size(80, 96);
            this.pbOpponentCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbOpponentCard.TabIndex = 3;
            this.pbOpponentCard.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(910, 332);
            this.Controls.Add(this.pbOpponentCard);
            this.Controls.Add(this.btnForfeit);
            this.Controls.Add(this.lblOpponentScore);
            this.Controls.Add(this.lblMyScore);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pbOpponentCard)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMyScore;
        private System.Windows.Forms.Label lblOpponentScore;
        private System.Windows.Forms.Button btnForfeit;
        private System.Windows.Forms.PictureBox pbOpponentCard;
    }
}

